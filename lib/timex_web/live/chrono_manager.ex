defmodule TimexWeb.ChronoManager do
    use GenServer

    def init(ui) do
        :gproc.reg({:p, :l, :ui_event})
        {
            :ok,
            %{
                ui_pid: ui,
                st: :paused,
                mode: :time,
                count: ~T[00:00:00.00]
            }
        }
    end

    def handle_info(:"top-left", %{ui_pid: ui, mode: :time} =state) do
        GenServer.cast(ui, {:set_time_display, "00:00.00" })

        {:noreply, state |> Map.put(:mode, :chrono)}
    end

    def handle_info(:"top-left", %{mode: :chrono} =state) do
        {:noreply, state |> Map.put(:mode, :time)}
    end

    def handle_info(_event, state), do: {:noreply, state}
end
