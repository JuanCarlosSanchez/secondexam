defmodule TimexWeb.IndigloManager do
    use GenServer

    def init(ui) do
        :gproc.reg({:p, :l, :ui_event})
        {:ok, %{ui_pid: ui, st: :indiglo_off, count: 0, timer: nil}}
    end

    def handle_info(:"top-right", %{ui_pid: ui, st: :indiglo_off} = state) do
        GenServer.cast(ui, :set_indiglo)
        {:noreply, state |> Map.put(:st, :indiglo_on)}
    end

    def handle_info(:"top-right", %{st: :indiglo_on} = state) do
        Process.send_after(self(), :tick, 2000)
        {:noreply, state |> Map.put(:st, :waiting)}
    end

    def handle_info(:tick, %{ui_pid: ui, st: :waiting} = state) do
        GenServer.cast(ui, :unset_indiglo)
        {:noreply, state |> Map.put(:st, :indiglo_off)}
    end

    #Exam
    def handle_info(:alarm, %{ui_pid: ui, st: :indiglo_on} = state) do
        Process.send_after(self(), :alarm_off, 1000)
        :gproc.send({:p, :l, :ui_event}, :alarm_off)
        GenServer.cast(ui, :unset_indiglo)
        {:noreply, %{state | st: :indiglo_off, count: 0}}
    end

    def handle_info(:alarm, %{ui_pid: ui, st: :waiting} = state) do
        Process.send_after(self(), :alarm_off, 1000)
        :gproc.send({:p, :l, :ui_event}, :alarm_off)
        GenServer.cast(ui, :unset_indiglo)
        {:noreply, %{state | st: :indiglo_off, count: 0}}
    end

    def handle_info(:alarm, %{ui_pid: ui, st: :indiglo_off} = state) do
        timer = Process.send_after(self(), :alarm_on, 1000)
        :gproc.send({:p, :l, :ui_event}, :alarm_on)
        GenServer.cast(ui, :set_indiglo)
        {:noreply, %{state | st: :indiglo_on, count: 1, timer: timer}}
    end

    def handle_info(:alarm_on, %{st: :indiglo_on, count: cuenta, timer: timer} = state) do
        if timer != nil do
           Process.cancel_timer(timer)
        end
       if cuenta <  9 do
            timer = Process.send_after(self(), :alarm_on_to_off, 1000)
            {:noreply, %{state | st: :indiglo_on, count: cuenta, timer: timer}}
       else
            :gproc.send({:p, :l, :ui_event}, :alarm_on_to_indigloOff)
            {:noreply, state}
       end
    end

    def handle_info(:alarm_on_to_off, %{ui_pid: ui, count: cuenta} = state) do
        cuenta = cuenta + 1
        timer = Process.send_after(self(), :alarm_off, 1000)
        :gproc.send({:p, :l, :ui_event}, :alarm_off)
        GenServer.cast(ui, :unset_indiglo)
        {:noreply, %{state | st: :indiglo_off, count: cuenta, timer: timer}}
    end

    def handle_info(:alarm_on_to_indigloOff, %{ui_pid: ui} = state) do
        GenServer.cast(ui, :unset_indiglo)
        {:noreply, %{state | st: :indiglo_off}}
    end

    def handle_info(:alarm_off, %{ui_pid: ui, st: :indiglo_off, count: cuenta, timer: timer} = state) do
        if timer != nil do
            Process.cancel_timer(timer)
         end
        cuenta = cuenta + 1
        timer = Process.send_after(self(), :alarm_on, 1000)
        :gproc.send({:p, :l, :ui_event}, :alarm_on)
        GenServer.cast(ui, :set_indiglo)
        {:noreply, %{state | st: :indiglo_on, count: cuenta, timer: timer}}
    end


    def handle_info(_event, state), do: {:noreply, state}
end
